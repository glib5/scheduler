from dataclasses import dataclass
from datetime import datetime, time
from importlib import import_module, reload
import logging
from json import load
from pathlib import Path
from time import sleep
from typing import Any, Generator, List, Literal


def set_logger(file:Path):
    """slightly altered version"""
    logging.basicConfig(
        level=logging.INFO, 
        format='%(asctime)s [%(name)s] %(message)s', 
        datefmt=Constants.datetime_fmt,
        handlers=(logging.StreamHandler(), 
                  logging.FileHandler(filename=file, mode="a")))
    logger = logging.getLogger("Scheduler") 
    global print
    print = logger.info
    return


@dataclass(frozen=True)
class Constants:
    int_to_dayname = {0:"Monday",
                      1:"Tuesday",
                      2:"Wednesday",
                      3:"Thursday",
                      4:"Friday",
                      5:"Saturday",
                      6:"Sunday"}
    frequencies = ("D", "W")
    date_fmt = "%d-%m-%Y"
    datetime_fmt = f"{date_fmt} %H:%M:%S"
    parent_folder = Path(__file__).parent
    static_folder = parent_folder / "static"
    modules_file = static_folder / "modules.json"
    log_folder = parent_folder / "logfiles"


@dataclass(frozen=True)
class ModuleInfo:
    name: str = None
    frequency: Literal["D", "W"] = None
    start_day: str = None
    start_time: time = None

    def __post_init__(self):
        assert isinstance(self.name, str) 
        assert self.frequency in Constants.frequencies
        if self.frequency == "W":
            assert isinstance(self.start_day, str)
            assert self.start_day in Constants.int_to_dayname.values()
        assert isinstance(self.start_time, time)

    @classmethod
    def fromdict(cls, d:dict):
        assert isinstance(d, dict)
        try:
            _name = d["name"]
            _freq = d["frequency"]
            _day = d["day"] if _freq != "D" else ""
            _time = d["time"]
        except KeyError:
            raise Exception("Missing some parameters")
        if not isinstance(_time, time):
            try:
                _hour = _time[0]
                _minute = _time[1]
                _time = time(hour=_hour, minute=_minute)
            except IndexError:
                raise Exception("'time' parameter is incorrect")
        return cls(name=_name, frequency=_freq, start_day=_day.capitalize(), start_time=_time)


class Scheduler:
    """
    basic scheduler for small scripts

    these scripts must implement a main function that will be executed
    """
    def __init__(self) -> None:
        self.modules_jsonfile =  Constants.modules_file
        self.today = datetime.today().date()
        self.date_str = self.today.strftime(Constants.date_fmt)
        set_logger(file=Constants.log_folder / f"{self.date_str}.log")
        self.cob = datetime.combine(date=self.today, time=time(hour=19, minute=0))
        self.today_modules = self._get_today_modules()
        self.runned_modules = Constants.log_folder / f"{self.date_str} runned.txt"
        if not self.runned_modules.exists():
            self.runned_modules.touch()

        print("Scheduler started ...")
        print(f"today modules {[info.name for info in self.today_modules]}")

    def _get_all_modules(self) -> Generator[ModuleInfo, Any, Any]:
        """creates all the modules objects, according to the info provided in the json file"""
        with self.modules_jsonfile.open() as f:
            modules_info = load(fp=f)
        return (ModuleInfo.fromdict(d) for d in modules_info)

    def _get_today_modules(self) -> List[ModuleInfo]:
        """returns all the modules that should run today"""
        today_num = self.today.weekday()
        today_name = Constants.int_to_dayname[today_num]
        today_modules: List[ModuleInfo] = []
        if today_num not in (6, 7):
            for module in self._get_all_modules():
                if module.frequency == "D":
                    today_modules.append(module)
                elif (module.frequency == "W") and (module.start_day == today_name):
                    today_modules.append(module) 
        return today_modules

    def _get_runned_modules(self) -> frozenset:
        return frozenset(self.runned_modules.read_text().split("\n"))

    def _wait(self):
        if self.today_modules:
            minutes = 15
            print(f"waiting {minutes} minutes ...")
            sleep(minutes*60)

    def _run_now_modules(self,  now:datetime):
        """runs all the modules scheduled before 'now' that were not run yet"""
        to_run:List[ModuleInfo] = [module for module in self.today_modules if module.start_time <= now.time()]
        to_exe:List[ModuleInfo] = []
        # check which modules should run
        if to_run:
            print(f"running modules {[module.name for module in to_run]}")
            already_run = self._get_runned_modules()
            for module in to_run:
                if module.name in already_run:
                    print(f"Module '{module.name}' was already run")
                    self.today_modules.remove(module)
                else:
                    to_exe.append(module)
        # actually run the modules
        if to_exe:
            if len(to_exe) == 1:
                out = [self._exe_module(to_exe[0])]
            else:
                out = tuple(map(self._exe_module, to_exe))
            for module, o in zip(to_exe, out):
                if not o:
                    print(f"module '{module.name}' completed successfully")
                    with self.runned_modules.open(mode="a") as f:
                        f.write(f"{module.name}\n")
                    self.today_modules.remove(module)
                else:
                    print(f"problems with module '{module.name}' -- will try again later")

    def _exe_module(self, module:ModuleInfo) -> int:
        """import a module and executes the main function in it"""
        _name = module.name
        try:
            _mod = import_module(name=f"modules.{_name}.{_name}")
        except ImportError:
            print(f"Problems in importing {_name}, bailing out")
            raise
        mod = reload(_mod)
        print(f"module '{_name}' loaded")
        try:
            _ = mod.main()
        except AttributeError:
            print(f"Module {_name} does not have a 'main' function, bailing out")
            quit()
        except:
            return 1 # problems
        return 0 # all good

    def mainloop(self):
        while 1:   
            if not self.today_modules:
                print("No more modules to run today")
                break
            now = datetime.now()
            self._run_now_modules(now)
            if self.cob < now:
                print(f"CoB reached ({self.cob.strftime(Constants.datetime_fmt)}), that's it for today")
                break
            self._wait()
        return 0


if __name__ == "__main__":
    from os import system
    _ = system("cls")
    Scheduler().mainloop()