from random import choice, randrange
from string import ascii_letters


def set_logger():
    """
    set new logger for stream and file
    - overrides "print"
    - to be inserted and called in every script you wish to be logged
    - file is produced when calling from cmd prompt 
      - (new log every time)
    """
    import logging
    from pathlib import Path
    name = Path(__file__).stem
    logging.basicConfig(
        level=logging.INFO, 
        format='%(asctime)s [%(name)s] %(message)s', 
        datefmt="%d/%m/%Y %H:%M:%S",
        handlers=(logging.StreamHandler(), 
                  logging.FileHandler(filename=f"{name}.log", mode="w")))
    logger = logging.getLogger(name)
    global print
    print = logger.info
    return 


def get_word():
    return "".join(choice(ascii_letters) for _ in range(randrange(3, 8)))


def main():
    set_logger()
    print(get_word())
    return 0