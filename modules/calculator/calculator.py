def set_logger():
    """
    set new logger for stream and file
    - overrides "print"
    - to be inserted and called in every script you wish to be logged
    - file is produced when calling from cmd prompt 
      - (new log every time)
    """
    import logging
    from pathlib import Path
    name = Path(__file__).stem
    logging.basicConfig(
        level=logging.INFO, 
        format='%(asctime)s [%(name)s] %(message)s', 
        datefmt="%d/%m/%Y %H:%M:%S",
        handlers=(logging.StreamHandler(), 
                  logging.FileHandler(filename=f"{name}.log", mode="w")))
    logger = logging.getLogger(name)
    global print
    print = logger.info
    return 


def add(a, b):
    return a + b

def sub(a, b):
    return a - b


def main():
    set_logger()

    a = 1
    b = 2

    print(add(a, b))
    print(sub(a, b))

    return 0