# scheduler

basic scheduler for small scripts

## usage

- create a nice script you wish to be scheduled
- add a 'main' function to it, taking no arguments
- edit the .json file in the static folder to add a scheduling time for your script
- add the script and all the relative files in a folder named exactly like the script itself, in the 'modules' folder

run the sched.py file

- all the scripts specified in the .json file will be run at the scheduled time
- a single logfile is produced
  - all you print in the scripts will be logged (print = logging.info)
- scripts that complete successfully are not run again on the same day
- every 15 minutes a check for new scripts to run is performed until cob (19:00)
